const Sequelize = require('sequelize')

class Client {
  constructor ({ username, password, database, host = 'localhost', prefix = 'wp_' }) {
    this.sequelize = new Sequelize(database, username, password, {
      host,
      dialect: 'mysql'
    })

    const models = [
      'comments',
      'options',
      'posts',
      'users'
    ]
    models.forEach((modelName) => {
      this[modelName] = this.sequelize.define(`${prefix}${modelName}`, require(`./${modelName}`), { timestamps: false })
    })

    this.posts.hasMany(this.comments, { as: 'comments', foreignKey: 'comment_post_ID' })
    this.comments.belongsTo(this.posts, { as: 'post', foreignKey: 'comment_post_ID' })

    this.users.hasMany(this.posts, { as: 'posts', foreignKey: 'post_author' })
    this.posts.belongsTo(this.users, { as: 'author', foreignKey: 'post_author' })

    this.users.hasMany(this.comments, { as: 'comments', foreignKey: 'user_id' })
    this.comments.belongsTo(this.users, { as: 'author', foreignKey: 'user_id' })
  }

  connect () {
    return this.sequelize.sync()
  }
}

module.exports = Client
