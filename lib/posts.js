const Sequelize = require('sequelize')

module.exports = {
  ID: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  post_author: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    allowNull: false,
    defaultValue: 0
  },
  post_date: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  post_date_gmt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  post_content: {
    type: Sequelize.TEXT('long'),
    allowNull: false
  },
  post_title: {
    type: Sequelize.TEXT,
    allowNull: false,
    defaultValue: null
  },
  post_excerpt: {
    type: Sequelize.TEXT,
    allowNull: false,
    defaultValue: null
  },
  post_status: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: 'publish'
  },
  comment_status: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: 'open'
  },
  ping_status: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: 'open'
  },
  post_password: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  post_name: {
    type: Sequelize.STRING(200),
    allowNull: false
  },
  to_ping: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  pinged: {
    type: Sequelize.TEXT,
    allowNull: false,
    defaultValue: null
  },
  post_modified: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  post_modified_gmt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  post_content_filtered: {
    type: Sequelize.TEXT('long'),
    allowNull: false
  },
  post_parent: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    allowNull: false,
    defaultValue: 0
  },
  guid: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  menu_order: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    defaultValue: 0
  },
  post_type: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: 'post'
  },
  post_mime_type: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  comment_count: {
    type: Sequelize.BIGINT(20),
    allowNull: false,
    defaultValue: 0
  }
}
