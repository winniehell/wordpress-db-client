const Sequelize = require('sequelize')

module.exports = {
  comment_ID: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  comment_post_ID: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    allowNull: false
  },
  comment_author: {
    type: Sequelize.TEXT('tiny'),
    allowNull: false
  },
  comment_author_email: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  comment_author_url: {
    type: Sequelize.STRING(200),
    allowNull: false
  },
  comment_author_IP: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  comment_date: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  comment_date_gmt: {
    type: Sequelize.DATE,
    allowNull: false,
    defaultValue: '0000-00-00 00:00:00'
  },
  comment_content: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  comment_karma: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    defaultValue: 0
  },
  comment_approved: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: '1'
  },
  comment_agent: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  comment_type: {
    type: Sequelize.STRING(20),
    allowNull: false
  },
  comment_parent: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    allowNull: false
  },
  user_id: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    allowNull: false,
    defaultValue: 0
  }
}
