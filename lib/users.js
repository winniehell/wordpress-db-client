const Sequelize = require('sequelize')

module.exports = {
  ID: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  user_login: {
    type: Sequelize.STRING(60),
    allowNull: false
  },
  user_pass: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  user_nicename: {
    type: Sequelize.STRING(50),
    allowNull: false
  },
  user_email: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  user_url: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  user_registered: {
    type: Sequelize.DATE,
    allowNull: false
  },
  user_activation_key: {
    type: Sequelize.STRING(255),
    allowNull: false
  },
  user_status: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    defaultValue: 0
  },
  display_name: {
    type: Sequelize.STRING(250),
    allowNull: false
  }
}
