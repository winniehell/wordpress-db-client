const Sequelize = require('sequelize')

module.exports = {
  option_id: {
    type: Sequelize.BIGINT(20).UNSIGNED,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  option_name: {
    type: Sequelize.STRING(191),
    allowNull: true
  },
  option_value: {
    type: Sequelize.TEXT('long'),
    allowNull: false
  },
  autoload: {
    type: Sequelize.STRING(20),
    allowNull: false,
    defaultValue: 'yes'
  }
}
