const WordpressDbClient = require('.')

const config = require('./example-config.json')
const client = new WordpressDbClient(config)

client.connect()
  .then(() => client.options.findOne({
    where: {
      option_name: 'blogname'
    }
  }))
  .then(option => console.log(`

${option.get({ plain: true }).option_value}

  `))
  .then(() => client.posts.findOne({
    where: {
      post_status: 'publish'
    },
    order: [
        ['post_modified', 'DESC']
    ]
  })
  )
  .then(postInstance => {
    const post = postInstance.get({ plain: true })
    postInstance.getComments({
      limit: 1,
      where: {
        comment_approved: '1'
      },
      order: [
        ['comment_date', 'DESC']
      ]
    }).then(comments => {
      comments.map(instance => instance.get({ plain: true })).forEach(comment => {
        console.log(`

${post.ID}: ${post.post_title}

${comment.comment_author}: ${comment.comment_content}

        `)
      })
    })
  })
  .then(() => client.users.findOne())
  .then(userInstance => userInstance.getComments())
  .then(userComments => {
    console.log(`

${JSON.stringify(userComments[0].get({ plain: true }), null, 2)}

    `)
  })
